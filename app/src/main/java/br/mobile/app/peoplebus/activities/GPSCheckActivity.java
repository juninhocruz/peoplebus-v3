package br.mobile.app.peoplebus.activities;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import br.mobile.app.peoplebus.R;
import br.mobile.app.peoplebus.util.GPSTracker;

import static br.mobile.app.peoplebus.util.GPSTracker.LocationStatusListener;
import static br.mobile.app.peoplebus.util.GPSTracker.getInstance;

public class GPSCheckActivity extends ActionBarActivity {
    private GPSTracker gps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gpscheck);

        getSupportActionBar().hide();

        gps = getInstance(getApplicationContext());

        gps.setLocationStatusListener(new LocationStatusListener() {
            @Override
            public void onImpossibilityToLocate() {
                //Não é possível localizar
                Log.i("pbus", "Não pode localizar....");

                //Mensagem de alerta de Suporte à localização desativados.
                TextView textView = (TextView) findViewById(R.id.tv_gps_check);
                textView.setText(getString(R.string.gps_alert_message_disabled));

                textView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(intent);
                    }
                });
            }

            @Override
            public void afterStartUpdatesGPS() {
                Intent intent = new Intent(GPSCheckActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });

        gps.startUpdatesGPS();
    }

    @Override
    protected void onResume() {
        super.onResume();
        gps.startUpdatesGPS();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_gpsverify, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
