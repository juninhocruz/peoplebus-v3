package br.mobile.app.peoplebus.util;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

/**
 * Created by CRUZ JR, A.C.V. on 12/03/15.
 * Esta classe implementa a integração dos módulos de localização
 */
public class GPSTracker extends Service implements LocationListener {
    /**
     *  Menor distancia para atualizar, em metros.
     */
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10;

    /**
     * Menor intervalo de tempo para atualizar, em milisegundos.
     */
    private static final long MIN_TIME_BW_UPDATES = 1000; //1 segundo

    private Context context = null;
    private static GPSTracker gpsTracker;

    private LocationStatusListener locationStatusListener;
    private LocationEventListener locationEventListener;
    private Location location;
    protected LocationManager locationManager;

    private boolean isValidLatitudeAndLongitude = false;

    private GPSTracker(Context context) {
        this.context = context;

        setLocationManager();
    }

    public static GPSTracker getInstance(Context context) {
        if (gpsTracker == null)
            gpsTracker = new GPSTracker(context);
        else
            gpsTracker.context = context;

        return gpsTracker;
    }

    /**
     * Este método calcula a distância (em metros) entre dois pontos cartesianos.
     * @param initialLatitude Double Latitude inicial
     * @param initialLongitude Double Longitude inicial
     * @param finalLatitude Double Latitude final
     * @param finalLongitude Double Longitude final
     * @return Double Distancia (em metros)
     */
    public static double getDistance(double initialLatitude, double initialLongitude, double finalLatitude, double finalLongitude) {
        double distance;

        Location pontoA = new Location("Ponto A");
        Location pontoB = new Location("Ponto B");
        pontoA.setLatitude(initialLatitude);
        pontoA.setLongitude(initialLongitude);
        pontoB.setLatitude(finalLatitude);
        pontoB.setLongitude(finalLongitude);
        distance = pontoA.distanceTo(pontoB);

        return distance;
    }

    private void setLocationManager() {
        this.locationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
    }

    /**
     * Este método inicia a coleta de dados de localização
     */
    public void startUpdatesGPS() {
        //Verificando status do GPS e da Internet
        /*
      Sinaliza o status do GPS
     */
        boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        /*
      Sinaliza o status da Internet
     */
        boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);


        if ((!isGPSEnabled) && (!isNetworkEnabled)) {
            if(locationStatusListener != null)
                locationStatusListener.onImpossibilityToLocate();
            else
                Log.e("LocationStatusListener", "LocationStatusListener não foi criado.");
        }
        else {
            //Pegando localização pela internet
            if (isNetworkEnabled) {
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                Log.d("pbus", "Localização por internet ativada");
            }

            //Pegando localização pelo GPS
            if (isGPSEnabled) {
                if (location == null) {
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    Log.d("pbus", "Localização por GPS ativada");
                }
            }

            locationStatusListener.afterStartUpdatesGPS();
        }
    }

    /**
     * Este método interrompe as coletas de localização
     */
    public void stopUpdatesGPS() {
        if(locationManager != null) {
            locationManager.removeUpdates(GPSTracker.this);
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onLocationChanged(Location location) {
        this.location = location;
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();

        //Este trecho grava a localização encontrada no LocalStorage
        if (latitude != 0.0 && longitude != 0.0) {
            LocalStorage localStorage = LocalStorage.getInstance(context);
            localStorage.addToStorage("latitude", latitude);
            localStorage.addToStorage("longitude", longitude);
            Log.d("pbus", "Localizei\nLatitude: " + latitude + "\nLongitude: " + longitude);
            isValidLatitudeAndLongitude = true;
            stopUpdatesGPS();

            if(locationEventListener != null)
                locationEventListener.onLocationChanged();
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    public void setLocationStatusListener(LocationStatusListener locationStatusListener) {
        this.locationStatusListener = locationStatusListener;
    }

    public void setLocationEventListener(LocationEventListener locationEventListener) {
        this.locationEventListener = locationEventListener;
    }

    public boolean isValidLatitudeAndLongitude() {
        return isValidLatitudeAndLongitude;
    }

    public interface LocationStatusListener {
        /**
         * Neste método deve ser implementado as ações para o tratamento da impossibilidade de localizar. Isto acontece quando a localização por GPS e pela internet estão desativados no dispositivo.
         */
        public abstract void onImpossibilityToLocate();

        /**
         * Neste método deve ser implementado as ações para após o início das coletas de localização.
         */
        public abstract void afterStartUpdatesGPS();
    }

    public interface LocationEventListener {
        /**
         * Neste método deve ser implementado as ações para o instante em que se encontra uma localização válida.
         */
        public void onLocationChanged();
    }
}
