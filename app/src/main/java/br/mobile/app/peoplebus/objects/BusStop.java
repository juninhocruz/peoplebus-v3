package br.mobile.app.peoplebus.objects;

/**
 * Created by CRUZ JR, A.C.V. on 10/03/15.
 * Esta classe descreve um objeto Parada de ônibus
 */
public class BusStop {
    private int id;
    private String description;
    private double latitude, longitude;
    private double relativeDistance;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getRelativeDistance() {
        return relativeDistance;
    }

    public void setRelativeDistance(double relativeDistance) {
        this.relativeDistance = relativeDistance;
    }
}
