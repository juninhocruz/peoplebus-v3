package br.mobile.app.peoplebus.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

import com.google.gson.Gson;

/**
 * Persistência de dados
 * @author Richard Lopes
 */
public class LocalStorage {
    public static LocalStorage localStorage;

	private static final String SETTINGS = "settings";

	/**
	 * {@link #LOGIN_NAME} <p>Return: String
	 */
	public static final String LOGIN_NAME = "login";
	/**
	 * {@link #PASSWORD} <p>Return: String
	 */
	public static final String PASSWORD = "password";
	/**
	 * {@link #IS_LOGGED} <p>Return: Boolean
	 */
	public static final String IS_LOGGED = "status_login";
	/**
	 * {@link #BUSLINE} <p>Return: {@link br.mobile.app.peoplebus.objects.BusLine}
	 */
    public static final String BUSLINE = "busline";
    /**
     * {@link #BUSSTOP} <p>Return: {@link br.mobile.app.peoplebus.objects.BusStop}
     */
    public static final String BUSSTOP = "busstop";
	
	private SharedPreferences settings;
	private Editor editor;
	
	public LocalStorage(Context context) {
		settings = context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE);
		editor = settings.edit();
	}
	
	public static LocalStorage getInstance(Context context)	{
		if(localStorage == null)
			localStorage = new LocalStorage(context);
		return localStorage;
	}
	
	public void addToStorage(String key, String value)	{
		editor.putString(key, value);
		editor.commit();
	}
	
	public void addToStorage(String key, int value)	{
		editor.putInt(key, value);
		editor.commit();
	}
	
	/**
	 * Persiste um valor <code>boolean</code> sob uma chave pré-definida. 
	 * @param key Chave pré-definida.
	 * @param value Valor
	 */
	public void addToStorage(String key, boolean value)	{
		editor.putBoolean(key, value);
		editor.commit();
	}

    /**
     * Persiste um valor <code>double</code> sob uma chave pré-definida.
     * @param key Chave pré-definida.
     * @param value Valor
     */
    public void addToStorage(String key, double value) {
        editor.putString(key, "" + value);
        editor.commit();
    }

    /**
     * Persiste um valor <code>Object</code> sob uma chave pré-definida.
     * @param key Chave pré-definida.
     * @param value Valor
     */
    public <T> void addToStorage(String key, T value) {
        Gson gson = new Gson();
        String json = gson.toJson(value);
        editor.putString(key, json);
        editor.commit();
    }

    /**
     * Retorna um Objeto que foi armazenado utilizando uma chave especícia <br/>
     * @param key Chave pré-definida.
     * @return Dado persistido. Caso não haja, retorna <code>null</code>
     */
    public <T> T getObjectFromStorage(String key, Class<T> clazz) {
        String json = settings.getString(key, null);
        Gson gson = new Gson();

        T object = gson.fromJson(json, clazz);

        return object;
    }
	
	/**
	 * Retorna uma string que foi armazenada utilizando uma chave especícia <br/>
	 * @param key Chave pré-definida. {@link #LOGIN_NAME} {@link #PASSWORD}
	 * @return Dado persistido. Caso não haja, retorna <code>null</code>
	 */
	public String getStringFromStorage(String key) {
		return settings.getString(key, null);
	}
	
	/**
	 * Retorna um inteiro que foi armazenado utilizando uma chave especícia <br/>
	 * @param key Chave pré-definida
	 * @return Dado persistido
	 */
	public int getIntFromStorage(String key) {
		return settings.getInt(key, 0);
	}
	
	/**
	 * Retorna um valor booleano foi armazenado utilizando uma chave especícia <br/>
	 * @param key Chave pré-definida.
	 * @return Dado persistido
	 */
	public boolean getBooleanFromStorage(String key)
	{
		return settings.getBoolean(key, false);
	}

    /**
     * Retorna um valor double foi armazenado utilizando uma chave especícia <br/>
     * @param key Chave pré-definida.
     * @return Dado persistido
     */
    public double getDoubleFromStorage(String key)
    {
        String str = settings.getString(key, null);
        if (str != null)
            try {
                return Double.parseDouble(str);
            } catch (Exception e) {
                //Falha na conversão String para Double
                Log.e("pbus", "Falha na conversão de String para Double em LocalStorage.getDoubleFromStorage");
                return 0.0;
            }
        else
            return 0.0;
    }
}
