package br.mobile.app.peoplebus.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;

import br.mobile.app.peoplebus.R;
import br.mobile.app.peoplebus.util.LocalStorage;


public class SplashActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        //Ocultando a ActionBar
        getSupportActionBar().hide();

        LocalStorage localStorage = LocalStorage.getInstance(getApplicationContext());
        localStorage.addToStorage("latitude", 0.0);
        localStorage.addToStorage("longitude", 0.0);

        Handler handler = new Handler();
        //SplashMagic - Trecho abaixo executa uma thread que, após 3s, lança a Activity de Login
        handler.postDelayed(new Runnable() {
            public void run() {
                Intent intent = new Intent(getBaseContext(), GPSCheckActivity.class);

                startActivity(intent);
                finish();
            }
        }, 3000);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_splash, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
