package br.mobile.app.peoplebus.objects;

/**
 * Created by CRUZ JR, A.C.V. on 10/03/15.
 * Classe descreve o objeto Linha de ônibus
 */
public class BusLine {
    private int id, busStops,primaryBusStopId;
    private String name, description;

    public BusLine() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBusStops() {
        return busStops;
    }

    public void setBusStops(int busStops) {
        this.busStops = busStops;
    }

    public int getPrimaryBusStopId() {
        return primaryBusStopId;
    }

    public void setPrimaryBusStopId(int primaryBusStopId) {
        this.primaryBusStopId = primaryBusStopId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
