package br.mobile.app.peoplebus.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import br.mobile.app.peoplebus.R;
import br.mobile.app.peoplebus.objects.BusStop;

/**
 * Created by CRUZ JR, A.C.V. on 10/03/15.
 * Essa classe tem a utilidade de organizar objetos BusStop para exibi-los em uma ListView
 */
public class BusStopAdapter extends ArrayAdapter<BusStop> {
    private Context context;
    private List<BusStop> objects;
    private double latitude, longitude;

    /**
     * Construtor da classe
     * @param context Contexto da aplicação
     * @param objects Lista de objetos para o ArrayAdapter
     * @param latitude latitude referência para cálculo da distância
     * @param longitude longitude referência para cálculo da distância
     */
    public BusStopAdapter(Context context, List<BusStop> objects, double latitude, double longitude) {
        super(context, 0, objects);
        this.context = context;
        this.objects = objects;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    /**
     * Construtor encadeado
     * @param context Contexto da aplicação
     * @param objects Lista de objetos para o ArrayAdapter
     */
    public BusStopAdapter(Context context, List<BusStop> objects) {
        super(context, 0, objects);
        this.context = context;
        this.objects = objects;
        this.latitude = 0.0;
        this.longitude = 0.0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        BusStop busStop = objects.get(position);

        if(view == null)
            view = LayoutInflater.from(context).inflate(R.layout.item_list_busstop, null);

        //Trecho abaixo preenche os valores de um item da lista.
        TextView tvName = (TextView) view.findViewById(R.id.item_busstop_name);
        TextView tvDistance = (TextView) view.findViewById(R.id.item_busstop_distance);

        tvName.setText(busStop.getDescription());
        if (latitude == 0.0 && longitude == 0.0) {
            tvDistance.setText("");
        }
        else {
            tvDistance.setText("" + (int) GPSTracker.getDistance(latitude, longitude, busStop.getLatitude(), busStop.getLongitude()) + " metros");
        }

        return view;
    }
}
