package br.mobile.app.peoplebus.connection;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;

/**
 * Created by CRUZ JÚNIOR, A.C.V. on 10/03/15.
 * Esta classe descreve uma requisição, com um ou mais parâmetros
 */
public class Request {
    private ArrayList<NameValuePair> parameters;

    public Request() {
        parameters = new ArrayList<NameValuePair>();
    }

    /**
     * Este método adiciona um parâmetro à requisição
     * @param label Nome do parâmetro
     * @param value Valor do Parâmetro
     */
    public void addParameter(String label, String value) {
        parameters.add(new BasicNameValuePair(label, value));
    }

    public ArrayList<NameValuePair> getParameters() {
        return parameters;
    }
}