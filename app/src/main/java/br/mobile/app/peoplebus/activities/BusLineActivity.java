package br.mobile.app.peoplebus.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import br.mobile.app.peoplebus.R;
import br.mobile.app.peoplebus.connection.ServiceNamesPeopleBus;
import br.mobile.app.peoplebus.objects.BusLine;
import br.mobile.app.peoplebus.util.BusLineAdapter;
import br.mobile.app.peoplebus.util.LocalStorage;
import library.mobile.br.httpwebrequestlibrary.HttpWebRequest;
import library.mobile.br.httpwebrequestlibrary.ResponseListener;


public class BusLineActivity extends ActionBarActivity {
    private ProgressDialog progressDialog;

    private ListView listView;

    private BusLineAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bus_line);

        progressDialog = ProgressDialog.show(this, "", getString(R.string.loading));

        listView = (ListView) findViewById(R.id.list_busline);
        listView.setAdapter(new BusLineAdapter(getApplication(), new ArrayList<BusLine>()));
        listView.setOnItemClickListener(new OnBusLineItemClickListener());

        requestBusLinesList();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_bus_line, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Método faz a requisição da lista de linhas de ônibus
     */
    private void requestBusLinesList() {
        Log.d("pbus", "Preparando requisição");

        ResponseListener responseListener = new ResponseListener() {
            @Override
            public void onSuccess(String answer) {
                try {
                    JSONObject jsonObject = new JSONObject(answer);
                    fillBusLinesList(jsonObject.getString("data"));
                } catch (JSONException e) {
                    Log.e("pbus","JSONObject contém erro.\n" + e.getMessage());
                    Toast.makeText(getApplicationContext(), getString(R.string.server_error_message), Toast.LENGTH_LONG).show();
                }
            }
        };

        HttpWebRequest httpWebRequest = new HttpWebRequest(ServiceNamesPeopleBus.SERVICE_GET_BUS_LINES_LIST, new HashMap<String, Object>(), HttpWebRequest.HttpRequestMethod.POST);

        httpWebRequest.setResponseListener(responseListener);
        httpWebRequest.setAutoRetry(true);
        httpWebRequest.execute();
    }

    /**
     * Método preenche a lista de linhas de ônibus a partir de um JSONArray de entrada
     * @param data JSONArray contendo todas as linhas de ônibus a serem inseridas na ListView.
     */
    private void fillBusLinesList(String data) {
        if(data == null || data.equals("")) {
            Log.e("pbus","JSON contém erro.\n" + data);
            Toast.makeText(getApplicationContext(), getString(R.string.server_error_message), Toast.LENGTH_LONG).show();
        }

        try {
            JSONArray jsonArray = new JSONArray(data);
            ArrayList<BusLine> list = new ArrayList<>();

            for(int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                BusLine busLine = new BusLine();
                busLine.setId(jsonObject.getInt("id"));
                busLine.setName(jsonObject.getString("name"));
                busLine.setDescription(jsonObject.getString("description"));
                busLine.setBusStops(jsonObject.getInt("busstops"));
                busLine.setPrimaryBusStopId(jsonObject.getInt("primary_busstop_id"));

                Log.d("pbus", "item " + i + ": BusLine " + busLine.getName());
                list.add(busLine);
            }

            adapter = new BusLineAdapter(getApplicationContext(), list);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    listView.setAdapter(adapter);
                }
            });

            progressDialog.dismiss();

        } catch (JSONException e) {
            Log.e("pbus","JSON contém erro.\n" + e.getMessage());
        }
    }

    /**
     * Método desconecta o usuário e exibe a tela de login
     */
    private void logout() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    /**
     * Esta classe contém as instruções para um item clicado na ListView de Linhas de Ônibus
     */
    class OnBusLineItemClickListener implements AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            BusLine busLine = (BusLine) listView.getItemAtPosition(position);
            Log.d("pbus", "id busline = " + busLine.getId());
            LocalStorage.getInstance(getApplication()).addToStorage(LocalStorage.BUSLINE, busLine);

            Intent intent = new Intent(BusLineActivity.this, BusStopActivity.class);
            startActivity(intent);
        }
    }
}

