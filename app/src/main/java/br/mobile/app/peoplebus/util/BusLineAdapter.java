package br.mobile.app.peoplebus.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import br.mobile.app.peoplebus.R;
import br.mobile.app.peoplebus.objects.BusLine;

/**
 * Created by CRUZ JR, A.C.V. on 10/03/15.
 * Essa classe tem a utilidade de organizar objetos BusLine para exibi-los em uma ListView
 */
public class BusLineAdapter extends ArrayAdapter<BusLine> {
    private Context context;
    private List<BusLine> objects;

    /**
     * Construtor da classe
     * @param context Contexto da aplicação
     * @param objects Lista de objetos para o ArrayAdapter
     */
    public BusLineAdapter(Context context, List<BusLine> objects) {
        super(context, 0, objects);
        this.context = context;
        this.objects = objects;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        BusLine busLine = objects.get(position);

        if(view == null)
            view = LayoutInflater.from(context).inflate(R.layout.item_list_busline, null);

        //Trecho abaixo preenche os valores de um item da lista.
        TextView tvName = (TextView) view.findViewById(R.id.item_busline_name);
        TextView tvDescription = (TextView) view.findViewById(R.id.item_busline_description);

        tvName.setText(busLine.getName());
        tvDescription.setText(busLine.getDescription());

        return view;
    }
}
