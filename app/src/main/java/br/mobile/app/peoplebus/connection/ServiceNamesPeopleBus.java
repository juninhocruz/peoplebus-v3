package br.mobile.app.peoplebus.connection;

public class ServiceNamesPeopleBus {
	public static final String URL_SERVER = "http://pbus.icomp.ufam.edu.br/peoplebus/";

    public static final String SERVICE_GET_BUS_LINES_LIST = URL_SERVER + "getBusLinesList";
    public static final String SERVICE_GET_BUS_STOPS_LIST = URL_SERVER + "getBusStopsListFromBusLine";
}
