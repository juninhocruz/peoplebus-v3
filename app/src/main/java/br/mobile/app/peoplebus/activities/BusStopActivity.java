package br.mobile.app.peoplebus.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import br.mobile.app.peoplebus.R;
import br.mobile.app.peoplebus.connection.ServiceNamesPeopleBus;
import br.mobile.app.peoplebus.objects.BusLine;
import br.mobile.app.peoplebus.objects.BusStop;
import br.mobile.app.peoplebus.util.BusStopAdapter;
import br.mobile.app.peoplebus.util.GPSTracker;
import br.mobile.app.peoplebus.util.LocalStorage;
import library.mobile.br.httpwebrequestlibrary.HttpWebRequest;
import library.mobile.br.httpwebrequestlibrary.ResponseListener;


public class BusStopActivity extends ActionBarActivity {
    private BusLine busLine;
    private ListView listView;
    private BusStopAdapter adapter;
    private ArrayList<BusStop> list;
    LocalStorage localStorage;

    private static final int MAX_BUS_STOPS_DISPLAYING = 5;

    private double latitude;
    private double longitude;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bus_stop);

        progressDialog = ProgressDialog.show(this, "" ,getString(R.string.loading));
        localStorage = LocalStorage.getInstance(getApplicationContext());

        busLine = localStorage.getObjectFromStorage(LocalStorage.BUSLINE, BusLine.class);

        listView = (ListView) findViewById(R.id.list_busstop);
        listView.setAdapter(new BusStopAdapter(getApplication(), new ArrayList<BusStop>()));
        listView.setOnItemClickListener(new OnBusStopItemClickListener());

        requestBusStopsList();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_bus_stop, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Este método solicita a lista de paradas de ônibus para preencher a ListView
     */
    private void requestBusStopsList() {
        Log.d("pbus", "preparando requisição");
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put("id", busLine.getId());

        ResponseListener responseListener = new ResponseListener() {
            @Override
            public void onSuccess(String answer) {
                try {
                    JSONObject jsonObject = new JSONObject(answer);
                    receiveBusStopsList(jsonObject.getString("data"));
                } catch (JSONException e) {
                    Log.e("pbus", "JSONObject contém erro.\n" + e.getMessage());
                    Toast.makeText(getApplicationContext(), getString(R.string.server_error_message), Toast.LENGTH_LONG).show();
                }
            }
        };

        HttpWebRequest httpWebRequest = new HttpWebRequest(ServiceNamesPeopleBus.SERVICE_GET_BUS_STOPS_LIST, parameters, HttpWebRequest.HttpRequestMethod.POST);

        httpWebRequest.setResponseListener(responseListener);
        httpWebRequest.setAutoRetry(true);
        httpWebRequest.execute();
    }

    /**
     * Método preenche ListView de Paradas de Ônibus a partir de uma entrada JSONArray
     * @param data JSONArray com todas as paradas a serem inseridas na ListView
     */
    private void receiveBusStopsList(String data) {
        if (data == null || data.equals("")) {
            Log.e("pbus", "A pesquisa de paradas não retornou dados.");
            Toast.makeText(getApplicationContext(), getString(R.string.server_error_message), Toast.LENGTH_LONG).show();
        }
        else {
            try {
                JSONArray jsonArray = new JSONArray(data);
                list = new ArrayList<>();

                for(int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    BusStop busStop = new BusStop();
                    busStop.setId(jsonObject.getInt("id"));
                    busStop.setDescription(jsonObject.getString("description"));
                    busStop.setLatitude(jsonObject.getDouble("latitude"));
                    busStop.setLongitude(jsonObject.getDouble("longitude"));
                    busStop.setRelativeDistance(jsonObject.getDouble("relative_distance"));
                    Log.d("pbus", "item " + i + ": BusStop " + busStop.getDescription());
                    list.add(busStop);
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.setMessage(getString(R.string.get_location_message));
                    }
                });

                getLocation();
            } catch (JSONException e) {
                Log.e("pbus", "JSON contém erro!\n" + e.getMessage());
            }
        }
    }

    /**
     * Este método verifica se há uma localização válida e, caso não haja, repete a verificação após um período determinado (0,5 segundos)
     */
    private void getLocation() {
        GPSTracker gpsTracker = GPSTracker.getInstance(getApplicationContext());


        if(gpsTracker.isValidLatitudeAndLongitude()) {
            getCoordinatesFromLocalStorage();
            fillBusStopsList();
        }
        else {
            gpsTracker.setLocationEventListener(new GPSTracker.LocationEventListener() {
                @Override
                public void onLocationChanged() {
                    getCoordinatesFromLocalStorage();
                    fillBusStopsList();
                }
            });
        }
    }

    private void getCoordinatesFromLocalStorage() {
        latitude = localStorage.getDoubleFromStorage("latitude");
        longitude = localStorage.getDoubleFromStorage("longitude");
        Log.d("pbus", "latitude: " + latitude + "\nlongitude: " + longitude);
    }

    private void fillBusStopsList() {
        //Ordenando a lista pela distância das paradas e adicionando ao ListView
        Collections.sort(list, new ComparatorBusStop());
        while (list.size() > MAX_BUS_STOPS_DISPLAYING) {
            list.remove(MAX_BUS_STOPS_DISPLAYING);
        }
        adapter = new BusStopAdapter(getApplicationContext(), list, latitude, longitude);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                listView.setAdapter(adapter);
            }
        });

        progressDialog.dismiss();
    }

    /**
     * Esta classe implementa as ações para um item clicado na ListView BusStop
     */
    class OnBusStopItemClickListener implements AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            //Aqui ficará a implementação para o click em um item na posição 'position' da ListView
            BusStop busStop = adapter.getItem(position);
            localStorage.addToStorage(LocalStorage.BUSSTOP, busStop);

            Intent intent = new Intent(BusStopActivity.this, CheckinActivity.class);
            startActivity(intent);
        }
    }

    /**
     * esta classe implementa um comparador para que as paradas sejam exibidas ordenadamente pela distância entre elas
     */
    class ComparatorBusStop implements Comparator<BusStop> {

        @Override
        public int compare(BusStop busStop1, BusStop busStop2) {
            double distanceBusStop1 = GPSTracker.getDistance(busStop1.getLatitude(), busStop1.getLongitude(), latitude, longitude);
            double distanceBusStop2 = GPSTracker.getDistance(busStop2.getLatitude(), busStop2.getLongitude(), latitude, longitude);

            //Retorna 1, 0 ou -1 se a primeira parada for, respectivamente, maior, igual ou menor a posição do usuário
            if (distanceBusStop1 == distanceBusStop2)
                return 0;
            else if (distanceBusStop1 > distanceBusStop2)
                return 1;
            else
                return -1;
        }
    }
}
