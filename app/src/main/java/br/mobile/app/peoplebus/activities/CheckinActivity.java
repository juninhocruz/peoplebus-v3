package br.mobile.app.peoplebus.activities;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import br.mobile.app.peoplebus.R;
import br.mobile.app.peoplebus.objects.BusLine;
import br.mobile.app.peoplebus.objects.BusStop;
import br.mobile.app.peoplebus.util.LocalStorage;


public class CheckinActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkin);

        Log.d("pbus", "Chegou na CheckinActivity.");
        LocalStorage localStorage = LocalStorage.getInstance(getApplicationContext());
        BusLine busLine = localStorage.getObjectFromStorage(LocalStorage.BUSLINE, BusLine.class);
        BusStop busStop = localStorage.getObjectFromStorage(LocalStorage.BUSSTOP, BusStop.class);
        Log.d("pbus", "Linha selecionada: " + busLine.getName() + "\nParada selecionada: " + busStop.getDescription());
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_checkin, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
